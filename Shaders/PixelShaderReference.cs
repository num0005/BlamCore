namespace BlamCore.Shaders
{
    public class PixelShaderReference
    {
        public string UpdbName;
        public PixelShaderHeader Header;
        public ShaderDebugHeader DebugHeader;
        public byte[] DebugData;
        public byte[] ShaderData;
    }
}
