using BlamCore.Serialization;

namespace BlamCore.Shaders
{
    [TagStructure(Size = 0x1C)]
    public class ShaderDebugHeader
    {
        public uint Magic;
        public uint StructureSize;
        public uint ShaderDataSize;
        public uint UpdbPointerOffset;
        public uint Unknown;
        public uint UnknownOffset;
        public uint CodeHeaderOffset;
    }
}
