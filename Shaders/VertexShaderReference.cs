namespace BlamCore.Shaders
{
    public class VertexShaderReference
	{
		public string UpdbName;
		public VertexShaderHeader Header;
		public ShaderDebugHeader DebugHeader;
		public byte[] DebugData;
		public byte[] ShaderData;
	}
}
