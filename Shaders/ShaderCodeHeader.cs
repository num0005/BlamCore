using BlamCore.Serialization;

namespace BlamCore.Shaders
{
    [TagStructure(Size = 0x8)]
    public class ShaderCodeHeader
    {
        public uint ConstantDataSize;
        public uint CodeDataSize;
    }
}
