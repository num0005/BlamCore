using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.TagResources
{
    [TagStructure(Name = "sound_resource_definition", Size = 0x14)]
    public class SoundResourceDefinition
    {
        public TagData Data;
    }
}