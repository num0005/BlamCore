namespace BlamCore.Ai
{
    public enum CharacterGrenadeType : short
    {
        HumanFragmentation,
        CovenantPlasma,
        BruteClaymore,
        Firebomb
    }
}
