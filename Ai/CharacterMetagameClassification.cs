namespace BlamCore.Ai
{
    public enum CharacterMetagameClassification : sbyte
    {
        Infantry,
        Leader,
        Hero,
        Specialist,
        LightVehicle,
        HeavyVehicle,
        GiantVehicle,
        StandardVehicle
    }
}