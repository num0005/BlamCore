namespace BlamCore.Ai
{
    public enum CharacterGrenadeTrajectoryType : short
    {
        Toss,
        Lob,
        Bounce
    }
}
