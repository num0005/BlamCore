using BlamCore.Cache;
using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x24)]
    public class CharacterEquipmentProperties
    {
        [TagField(Label = true)]
        public CachedTagInstance Equipment;
        public uint Unknown;
        public float UsageChance;
        public List<CharacterEquipmentUsageCondition> UsageConditions;
    }
}
