﻿using System;

namespace BlamCore.Ai
{
    [Flags]
    public enum CharacterMetagameFlags : byte
    {
        None,
        MustHaveActiveSeats = 1 << 0
    }
}