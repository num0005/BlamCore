namespace BlamCore.Ai
{
    public enum CharacterWeaponSpecialFireMode : short
    {
        None,
        Overcharge,
        SecondaryTrigger
    }
}
