using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x20)]
    public class CharacterSearchProperties
    {
        public CharacterSearchFlags Flags;
        public Bounds<float> SearchTime;
        public float SearchDistance;
        public Bounds<float> UncoverDistanceBounds;
        public Bounds<float> VocalizationTime;
    }
}
