namespace BlamCore.Ai
{
    public enum AiGlanceBehavior : short
    {
        None,
        GlanceSubjectShort,
        GlanceSubjectLong,
        GlanceCauseShort,
        GlanceCauseLong,
        GlanceFriendShort,
        GlanceFriendLong
    }
}
