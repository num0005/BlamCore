using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.Ai
{
    [TagStructure(Size = 0x18)]
    public class CharacterDialogueVariation
    {
        public CachedTagInstance Dialogue;
        [TagField(Label = true)]
        public StringId Name;
        public float Weight;
    }
}
