namespace BlamCore.Ai
{
    public enum AiDialoguePerception : short
    {
        None,
        Speaker,
        Listener
    }
}
