using BlamCore.Cache;
using System.IO;

namespace BlamCore.Legacy.HaloReach
{
    public class CacheFile : Halo3Retail.CacheFile
    {
        public CacheFile(FileInfo file, CacheVersion version = CacheVersion.HaloReach) :
            base(file, version)
        {
        }
    }
}
