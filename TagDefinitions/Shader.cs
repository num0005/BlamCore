using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "shader", Tag = "rmsh", Size = 0x4, MaxVersion = CacheVersion.HaloOnline106708)]
    [TagStructure(Name = "shader", Tag = "rmsh", Size = 0x10, MinVersion = CacheVersion.HaloOnline106708)]
    public class Shader : RenderMethod
    {
        public StringId Material;

        [TagField(Padding = true, Length = 12, MinVersion = CacheVersion.HaloOnline106708)]
        public byte[] Unused2;
    }
}