using BlamCore.Cache;
using BlamCore.Common;
using BlamCore.Serialization;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "device_arg_device", Tag = "argd", Size = 0x4, MinVersion = CacheVersion.Halo3ODST)]
    public class DeviceArgDevice : Device
    {
        public StringId ActionString;
    }
}
