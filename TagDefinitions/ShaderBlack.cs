using BlamCore.Serialization;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "shader_black", Tag = "rmbk", Size = 0x4)]
    public class ShaderBlack : RenderMethod
    {
        public uint Unknown1;
    }
}