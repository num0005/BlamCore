using BlamCore.Serialization;
using System.Collections.Generic;

namespace BlamCore.Scripting
{
    [TagStructure(Size = 0x34)]
    public class Script
    {
        [TagField(Length = 32)]
        public string ScriptName;
        public ScriptType Type;
        public ScriptValueType ReturnType;
        public uint RootExpressionHandle;
        public List<ScriptParameter> Parameters;
    }
}
